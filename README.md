# PeakData FE Assignment

Welcome! This is the PeakData FE assignment.


### Run Project

1. pull this repo
1. run:
```
composer install
npm install
npm run dev
php artisan serve
```
3. in the browser navigate to `http://127.0.0.1:8000/`
4. in order to go into dev-mode run `npm run watch`

**Note:** you need to create your `.env` file and specify DB connection there  

### Task

We challenge you need to implement the feature to comment on comments of comments...
You might already guessed it - we want an unlimited number of levels.
Please cover the backend functionality with tests.

On the `/` page there is a hardcoded structure of how it should approx looks like.
Please feel free to reuse or create your own one.
Also, don't forget to add some styles to make it looks better.


## You will be graded on:
 * Functionality. Is it possible to comment on comments.
 * Code quality does it meet best practices. 
 * Code tests.
   
## How to deliver
 * Make git commits. We like multiple commits with useful commit names ;)
 * Create a zip file of the project and send it to your contact person.
